package testcases;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testFramework.baseclasses.BaseTest;
import testcases.pageobjects.AlertsPage;
import testcases.pageobjects.AlertsWindowsPage;
import testcases.pageobjects.MainPage;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static testFramework.browser.BrowserUtils.*;

public class AlertsTest extends BaseTest {

    @DataProvider
    public Object[][] getExpectedData() throws IOException {
        List<HashMap<String, String>> data = getJsonDataToMap(System.getProperty("user.dir") + "//m.libera//src//test//resources//alert_test_expected_data.json");
        return new Object[][] {{data.get(0)}};
    }

    @Test(dataProvider = "getExpectedData")
    void AlertsPageTesting(HashMap<String, String> input) {
        MainPage mainPage = new MainPage();
        Assert.assertTrue(mainPage.isFormOpen(), "Main page is not opened.");

        mainPage.showAlertsFrameWindowsButton();
        mainPage.clickAlertsFrameWindowsButton();

        AlertsWindowsPage alertsWindowsPage = new AlertsWindowsPage();
        alertsWindowsPage.clickAlertsButton();

        AlertsPage alertsPage = new AlertsPage();
        Assert.assertTrue(alertsPage.isFormOpen(), "Alerts form is not appear on page.");

        alertsPage.clickShowAlert();
        Assert.assertEquals(alertText(), input.get("to_see_alert_text"), "Alert with text 'You clicked a button' is not open.");
        acceptAlert();
        Assert.assertFalse(isAlertDisplayed(), "AlertsAlert is displayed.");

        alertsPage.clickConfirmAlert();
        Assert.assertEquals(alertText(), input.get("confirm_box_alert_text"), "Alert with text 'Do you confirm action?' is not open.");
        acceptAlert();
        Assert.assertFalse(isAlertDisplayed(), "ConfirmAlert is displayed.");
        Assert.assertEquals(alertsPage.getResultAlertText(), input.get("selected_ok_alert_text"), "Alert result text is not valid.");

        alertsPage.clickShowPromtBoxAlert();
        Assert.assertEquals(alertText(), input.get("enter_name_alert_text"), "Alert with text 'Please enter your name' is not open.");
        String promptInput = alertsPage.getPromptBoxInput();
        inputAlertText(promptInput);
        acceptAlert();
        Assert.assertEquals(alertsPage.getResultPromptBoxText(), input.get("enter_name_result_alert_text") + promptInput, "Alert result text and entered text are ot the same.");

    }
}
