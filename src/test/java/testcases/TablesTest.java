package testcases;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testFramework.baseclasses.BaseTest;
import testcases.pageobjects.ElementsPage;
import testcases.pageobjects.MainPage;
import testcases.pageobjects.RegistrationForm;
import testcases.pageobjects.WebTablesPage;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class TablesTest extends BaseTest {

    @DataProvider
    public Object[][] getData() throws IOException {
        List<HashMap<String, String>> data = getJsonDataToMap(System.getProperty("user.dir") + "//m.libera//src//test//resources//users_data.json");
        return new Object[][] {{data.get(0)},{data.get(1)}};
    }

    @Test(dataProvider = "getData")
    public void TablesTesting(HashMap<String, String> input) {

        MainPage mainPage = new MainPage();
        Assert.assertTrue(mainPage.isFormOpen(), "Main page is not opened.");

        mainPage.elementsButtonClick();

        ElementsPage elementsPage = new ElementsPage();
        elementsPage.webTablesButtonClick();

        WebTablesPage webTablesPage = new WebTablesPage();
        Assert.assertTrue(webTablesPage.isFormOpen(), "Web Tables page is not opened.");

        webTablesPage.addNewRecordButtonClick();

        RegistrationForm registrationForm = new RegistrationForm();
        Assert.assertTrue(registrationForm.isFormOpen(), "Registration form is opened.");

        registrationForm.enterfirstName(input.get("firstname"));
        registrationForm.entersecondName(input.get("lastname"));
        registrationForm.enteruserEmail(input.get("email"));
        registrationForm.enteruserAge(input.get("age"));
        registrationForm.enteruserSalary(input.get("salary"));
        registrationForm.enteruserdepartment(input.get("department"));
        registrationForm.submitButtonClick();

        Assert.assertTrue(webTablesPage.isFormOpen(), "Registration form is still opened.");
        Assert.assertEquals(webTablesPage.lastRecordName(), input.get("firstname"), "Firstnames are not equals.");
        Assert.assertEquals(webTablesPage.getLastNameLastElement(), input.get("lastname"), "Lastnames are not equals.");
        Assert.assertEquals(webTablesPage.getEmailLastElement(), input.get("email"), "Mails are not equals.");
        Assert.assertEquals(webTablesPage.getAgeLastElement(), input.get("age"), "User's Ages are not equals.");
        Assert.assertEquals(webTablesPage.getSalaryLastElement(), input.get("salary"), "Salary are not equals.");
        Assert.assertEquals(webTablesPage.getDepartmentLastElement(), input.get("department"), "Dapertments are not equals.");
        webTablesPage.addNewRecordButtonClick();
        registrationForm.dismissButtonClick();

        webTablesPage.lastRecordDelete();
        Assert.assertNotEquals(webTablesPage.lastRecordName(), input.get("firstname"), "Record wasn't deleted - current last name matches last input name.");
        webTablesPage.navigateToMainPage();

    }

}
