package testcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import testFramework.baseclasses.BaseTest;
import testcases.pageobjects.AlertsWindowsPage;
import testcases.pageobjects.FramesPage;
import testcases.pageobjects.MainPage;
import testcases.pageobjects.NestedFramesPage;

public class IframeTest extends BaseTest {

    @Test
    void IFramePageTesting(){
        MainPage mainPage = new MainPage();
        Assert.assertTrue(mainPage.isFormOpen(), "Main page is not opened.");

        mainPage.showAlertsFrameWindowsButton();
        mainPage.clickAlertsFrameWindowsButton();

        AlertsWindowsPage alertsWindowsPage = new AlertsWindowsPage();
        alertsWindowsPage.clickNestedFramesButton();

        NestedFramesPage nestedFramesPage = new NestedFramesPage();
        Assert.assertTrue(nestedFramesPage.isFormOpen(), "Nested Frames form is not appear on page.");

        nestedFramesPage.goToIFrame();
        Assert.assertEquals(nestedFramesPage.getIFrameText(), "Parent frame", "Message 'Parent frame' is present on page");

        nestedFramesPage.goToChildFrame();
        Assert.assertEquals(nestedFramesPage.getIFrameText(), "Child Iframe", "Message 'Child frame' is present on page");
        nestedFramesPage.goToTopLayer();
        nestedFramesPage.showFrameMenuButton();
        nestedFramesPage.goToFramesOptions();

        FramesPage framesPage = new FramesPage();
        Assert.assertTrue(framesPage.isFormOpen(), "Frames form is not appear on page.");
        Assert.assertEquals(framesPage.getUpperIFrameText(), framesPage.getLowerIFrameText(), "Text in both iframes are not equal.");
    }

}
