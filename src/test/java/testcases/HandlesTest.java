package testcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import testFramework.baseclasses.BaseTest;
import testFramework.browser.BrowserUtils;
import testcases.pageobjects.*;


public class HandlesTest extends BaseTest {

    @Test
    void HandlesTesting() {
        MainPage mainPage = new MainPage();
        Assert.assertTrue(mainPage.isFormOpen(), "Main page is not opened.");

        mainPage.showAlertsFrameWindowsButton();
        mainPage.clickAlertsFrameWindowsButton();

        AlertsWindowsPage alertsWindowsPage = new AlertsWindowsPage();
        alertsWindowsPage.clickBrowserWindowsButton();

        BrowserWindowPage browserWindowPage = new BrowserWindowPage();
        Assert.assertTrue(browserWindowPage.isFormOpen(), "Browser window page is not opened.");

        browserWindowPage.clickNewTabButton();
        BrowserUtils.goToNewTab();

        NewTabPage newTabSamplePage = new NewTabPage();
        Assert.assertTrue(newTabSamplePage.isFormOpen(), "Sample page in new tab is not opened.");
        BrowserUtils.closeTab();
        BrowserUtils.switchToMainTab();
        Assert.assertTrue(browserWindowPage.isFormOpen(), "Browser window page is not opened.");

        browserWindowPage.openElementsMenu();
        browserWindowPage.clickLinksbutton();

        LinksPage linksPage = new LinksPage();
        Assert.assertTrue(linksPage.isFormOpen(), "linksPage is not opened.");

        linksPage.homePageButtonClick();
        BrowserUtils.goToNewTab();
        Assert.assertTrue(mainPage.isFormOpen(), "Main page is not opened.");
        BrowserUtils.switchToMainTab();
        Assert.assertTrue(linksPage.isFormOpen(), "linksPage is not opened.");

    }

}
