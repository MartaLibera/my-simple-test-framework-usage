package testcases.utilites;


import org.apache.commons.lang3.RandomStringUtils;

public class RandomTextGenerator {

    public static String getGeneratedText(int stringLen) {
        return RandomStringUtils.randomAlphanumeric(stringLen);
    }

}
