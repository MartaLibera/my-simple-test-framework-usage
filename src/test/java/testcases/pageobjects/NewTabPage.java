package testcases.pageobjects;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseForm;


public class NewTabPage extends BaseForm {

    public NewTabPage() {
        super(By.id("sampleHeading"), "Sample page in new tab opening");
    }

}
