package testcases.pageobjects;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;


public class LinksPage extends BaseForm {

    private final Button homePageButton = new Button(By.xpath("//a[contains(@target, '_blank') and contains(text(), 'Home')]"), "homePageButton");

    public LinksPage() {
        super(By.xpath("//div[contains(@class, 'main-header') and contains(text(), 'Links')]"), "LinksPage open");
    }

    public void homePageButtonClick(){
        homePageButton.showElement();
        homePageButton.clickOnElement();
    }

}
