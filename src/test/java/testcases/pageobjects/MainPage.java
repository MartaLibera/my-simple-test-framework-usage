package testcases.pageobjects;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;

public class MainPage extends BaseForm {

    private final Button alertsFrameWindowsbutton = new Button(By.xpath("//*[contains(text(), 'Alerts, Frame & Windows')]"), "AlertsFrameWindowsButton");
    private final Button elementsButton = new Button(By.xpath("//h5[contains(text(), 'Elements')]"), "elements Button");


    public MainPage() {
        super(By.className("home-body"), "Main page");
    }

    public void clickAlertsFrameWindowsButton(){
        alertsFrameWindowsbutton.clickOnElement();
    }

    public void showAlertsFrameWindowsButton () {
        alertsFrameWindowsbutton.showElement();
    }

    public void showElementsButton() {
        elementsButton.showElement();
    }

    public void elementsButtonClick() {
        showElementsButton();
        elementsButton.clickOnElement();
    }

}
