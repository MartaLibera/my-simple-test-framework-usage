package testcases.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;
import testFramework.browser.BrowserFactory;

public class WebTablesPage extends BaseForm {

    private final Button addNewRecordButton = new Button(By.id("addNewRecordButton"), "addNewRecordButton");
    private final Button mainPageButton = new Button(By.xpath("//a[contains(@href, 'https://demoqa.com/')]//img"), "mainPageButton");
    By registrationFormLocator = By.id("registration-form-modal");
    private final String lastRowRecordNameLocator = "//div[contains(@class, 'rt-tr')][last()]//div[contains(@class, 'rt-td')][1]";
    private final String lastNameLastRecordLocator = "//div[contains(@class, 'rt-tr')][last()]//div[contains(@class, 'rt-td')][2]";
    private final String emailLastRecordLocator = "//div[contains(@class, 'rt-tr')][last()]//div[contains(@class, 'rt-td')][4]";
    private final String ageLastRecordLocator = "//div[contains(@class, 'rt-tr')][last()]//div[contains(@class, 'rt-td')][3]";
    private final String salaryLastRecordLocator = "//div[contains(@class, 'rt-tr')][last()]//div[contains(@class, 'rt-td')][5]";
    private final String departmentLastRecordLocator = "//div[contains(@class, 'rt-tr')][last()]//div[contains(@class, 'rt-td')][6]";
    private final String deleteLastRecordLocator = "//span[contains(@id, 'delete-record')][last()]";


    public WebTablesPage() {
        super(By.xpath("//div[contains(@class, 'main-header') and contains(text(), 'Web Tables')]"), "WebTablesPage open");
    }

    public void addNewRecordButtonClick() {
        addNewRecordButton.clickOnElement();
    }

    public void navigateToMainPage() {
        mainPageButton.clickOnElement();
    }

    public String getLastNameLastElement(){
        WebElement field = BrowserFactory.createInstance().findElement(By.xpath(lastNameLastRecordLocator));
        return field.getText();
    }

    public String getEmailLastElement(){
        WebElement field = BrowserFactory.createInstance().findElement(By.xpath(emailLastRecordLocator));
        return field.getText();
    }

    public String getAgeLastElement(){
        WebElement field = BrowserFactory.createInstance().findElement(By.xpath(ageLastRecordLocator));
        return field.getText();
    }

    public String getSalaryLastElement(){
        WebElement field = BrowserFactory.createInstance().findElement(By.xpath(salaryLastRecordLocator));
        return field.getText();
    }

    public String getDepartmentLastElement(){
        WebElement field = BrowserFactory.createInstance().findElement(By.xpath(departmentLastRecordLocator));
        return field.getText();
    }

    public void lastRecordDelete(){
        Button recordDeleteBtn = new Button(By.xpath(deleteLastRecordLocator), "recordDeleteBtn");
        recordDeleteBtn.clickOnElement();
    }

    public String lastRecordName(){
        WebElement field = BrowserFactory.createInstance().findElement(By.xpath(lastRowRecordNameLocator));
        return field.getText();
    }

}
