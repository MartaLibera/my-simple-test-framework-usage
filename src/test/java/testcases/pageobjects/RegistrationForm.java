package testcases.pageobjects;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;
import testFramework.baseclasses.elements.TextBox;

public class RegistrationForm extends BaseForm {

    private final Button submittButton = new Button(By.id("submit"), "submittButton");
    private final Button closeButton = new Button(By.xpath("//button[contains(@class, 'close')]"), "closeButton");
    private final TextBox firstName = new TextBox(By.id("firstName"), "firstName field");
    private final TextBox lastName = new TextBox(By.id("lastName"), "lastName field");
    private final TextBox userEmail = new TextBox(By.id("userEmail"), "userEmail field");
    private final TextBox age = new TextBox(By.id("age"), "age field");
    private final TextBox salary = new TextBox(By.id("salary"), "salary field");
    private final TextBox department = new TextBox(By.id("department"), "department field");


    public RegistrationForm() {
        super(By.id("registration-form-modal"), "RegistrationForm");
    }

    public void submitButtonClick(){
        submittButton.clickOnElement();
    }

    public void dismissButtonClick(){
        closeButton.clickOnElement();
    }

    public void enterfirstName(String txt){
        firstName.isPresentAndEnterText(txt);
    }

    public void entersecondName(String txt){
        lastName.isPresentAndEnterText(txt);
    }

    public void enteruserEmail(String txt){
        userEmail.isPresentAndEnterText(txt);
    }

    public void enteruserAge(String user_age){
        age.isPresentAndEnterText(user_age);
    }


    public void enteruserSalary(String sal){
        salary.isPresentAndEnterText(sal);
    }

    public void enteruserdepartment(String dep){
        department.isPresentAndEnterText(dep);
    }


}
