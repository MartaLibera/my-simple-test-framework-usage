package testcases.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;
import testFramework.browser.BrowserFactory;

public class NestedFramesPage extends BaseForm {

    private final WebElement iframe = BrowserFactory.createInstance().findElement(By.id("frame1"));
    private final Button framesOptionButton = new Button(By.xpath("//span[contains(text(), 'Frames')]"), "Frames option open Button");
    private final String nestedFrameBodyTextCssLocator = "body";
    private final String iframeChildLocator = "//iframe[contains(@srcdoc, '<p>Child Iframe</p>')]";


    public NestedFramesPage() {
        super(By.xpath("//div[contains(@class, 'main-header') and contains(text(), 'Nested Frames')]"), "NestedFramesPage open");
    }

    public void goToIFrame() {
        BrowserFactory.createInstance().switchTo().frame(iframe);
    }

    public String getIFrameText() {
        WebElement body = BrowserFactory.createInstance().findElement(By.cssSelector(nestedFrameBodyTextCssLocator));
        return body.getText();
    }

    public void goToChildFrame() {
        WebElement iframeChild = BrowserFactory.createInstance().findElement(By.xpath(iframeChildLocator));
        BrowserFactory.createInstance().switchTo().frame(iframeChild);
    }

    public void goToFramesOptions(){
        framesOptionButton.clickOnElement();
    }

    public void goToTopLayer(){
        BrowserFactory.createInstance().switchTo().defaultContent();
    }

    public void showFrameMenuButton(){
        framesOptionButton.showElement();
    }

}
