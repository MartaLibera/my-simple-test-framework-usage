package testcases.pageobjects;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;
import testFramework.baseclasses.elements.SpanText;
import testcases.utilites.RandomTextGenerator;

public class AlertsPage extends BaseForm {

    private final Button alertButton = new Button(By.id("alertButton"), "alert button");
    private final Button alertConfirmButton = new Button(By.id("confirmButton"), "alert confirm button");
    private final Button alertPromtBoxButton = new Button(By.id("promtButton"), "alert promt box button");
    private final String resultAlertIdLocator = "confirmResult";
    private final String promptResultIdLocator = "promptResult";


    public AlertsPage() {
        super(By.xpath("//div[contains(@class, 'main-header') and contains(text(), 'Alerts')]"), "Alert page");
    }

    public void clickShowAlert() {
        alertButton.clickOnElement();
    }

    public void clickConfirmAlert() {
        alertConfirmButton.clickOnElement();
    }

    public void clickShowPromtBoxAlert() {
        alertPromtBoxButton.clickOnElement();

    }

    public String getResultAlertText(){
        SpanText confirmResult = new SpanText(By.id(resultAlertIdLocator), "confirm result text");
        return confirmResult.getElementText();
    }

    public String getPromptBoxInput(){
        RandomTextGenerator randomtext = new RandomTextGenerator();
        String randomInputText = randomtext.getGeneratedText(10);
        return randomInputText;
    }

    public String getResultPromptBoxText(){
        SpanText confirmResult = new SpanText(By.id(promptResultIdLocator), "confirm promptbox text");
        return confirmResult.getElementText();
    }

}
