package testcases.pageobjects;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;


public class AlertsWindowsPage extends BaseForm {

    private final Button alertsButton = new Button(By.xpath("//li[contains(@class, 'btn')]//span[contains(text(), 'Alerts')]"), "alerts button");
    private final Button nestedFramesButton = new Button(By.xpath("//li[contains(@class, 'btn')]//span[contains(text(), 'Nested Frames')]"), "nested Frames button");
    private final Button BrowserWindowsButton = new Button(By.xpath("//li[contains(@class, 'btn')]//span[contains(text(), 'Browser Windows')]"), "Browser Windows button");

    public AlertsWindowsPage() {
        super(By.xpath("//*[contains(text(), 'Alerts, Frame & Windows')] and contains(@class, 'main-header')"), "AlertWindows page open");
    }

    public void clickAlertsButton() {
        alertsButton.clickOnElement();
    }

    public void clickNestedFramesButton(){
        nestedFramesButton.clickOnElement();
    }

    public void clickBrowserWindowsButton(){
        BrowserWindowsButton.clickOnElement();
    }

}
