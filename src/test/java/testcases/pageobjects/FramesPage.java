package testcases.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import testFramework.baseclasses.BaseForm;
import testFramework.browser.BrowserFactory;
import testFramework.browser.BrowserUtils;

public class FramesPage extends BaseForm {

    private final WebElement upperIFrame = BrowserFactory.createInstance().findElement(By.id("frame1"));
    private final WebElement lowerIFrame = BrowserFactory.createInstance().findElement(By.id("frame2"));
    private final String iFrameBodyTextCssLocator = "body";

    public FramesPage() {
        super(By.xpath("//div[contains(@class, 'main-header') and contains(text(), 'Frames')]"), "NestedFramesPage open");
    }

    public String getIFrameText() {
        WebElement body = BrowserFactory.createInstance().findElement(By.cssSelector(iFrameBodyTextCssLocator));
        return body.getText();
    }

    public String getUpperIFrameText(){
        BrowserUtils.goToUpperFrame(upperIFrame);
        String text = getIFrameText();
        BrowserUtils.switchToDefaultContent();
        return text;
    }

    public String getLowerIFrameText(){
        BrowserUtils.goToLowerFrame(lowerIFrame);
        String text = getIFrameText();
        BrowserUtils.switchToDefaultContent();
        return text;
    }

}
