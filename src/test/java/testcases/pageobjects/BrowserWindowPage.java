package testcases.pageobjects;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;


public class BrowserWindowPage extends BaseForm {

    private final Button newTabButton = new Button(By.id("tabButton"), "newTabButton");
    private final Button elementsMenuOptionButton = new Button(By.xpath("//div[contains(@class, 'header-text') and contains(text(), 'Elements')]"), "Frames option open Button");
    private final Button linksMenuOptionButton = new Button(By.xpath("//span[contains(text(), 'Links')]"), "links Menu Option Button");

    public BrowserWindowPage() {
        super(By.xpath("//*[contains(@class, 'main-header') and contains(text(), 'Browser Windows')]"), "AlertWindows page open");
    }

    public void clickNewTabButton(){
        newTabButton.clickOnElement();
    }

    public void openElementsMenu(){
        elementsMenuOptionButton.clickOnElement();
    }

    public void clickLinksbutton(){
        linksMenuOptionButton.showElement();
        linksMenuOptionButton.clickOnElement();
    }



}
