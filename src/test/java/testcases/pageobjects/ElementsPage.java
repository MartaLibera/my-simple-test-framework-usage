package testcases.pageobjects;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseForm;
import testFramework.baseclasses.elements.Button;

public class ElementsPage extends BaseForm {

    private final Button webTablesButton = new Button(By.xpath("//span[contains(text(), 'Web Tables')]"), "Web Tables Menu Option Button");

    public ElementsPage() {
        super(By.xpath("//div[contains(@class, 'main-header') and contains(text(), 'Elements')]"), "ElementsPage open");
    }

    public void showWebTablesButtonClick() {
        webTablesButton.showElement();
    }
    public void webTablesButtonClick() {
        showWebTablesButtonClick();
        webTablesButton.clickOnElement();
    }

}
