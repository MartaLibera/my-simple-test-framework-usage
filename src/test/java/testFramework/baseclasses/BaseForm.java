package testFramework.baseclasses;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import testFramework.baseclasses.elements.Label;
import testFramework.browser.BrowserUtils;


public abstract class BaseForm {
    private By uniqueFormLocator;
    private String formName;
    private WebDriverWait waits;

    public BaseForm(By locator, String name) {
        uniqueFormLocator = locator;
        formName = name;
        waits = BrowserUtils.getWaits();
    }

    public Boolean isFormOpen() {
        try {
            waits.until(ExpectedConditions.presenceOfElementLocated(uniqueFormLocator));
            return true;
        }
        catch(TimeoutException e) {
            return false;
        }
    }

    private final Label getFormLabel() {
        return new Label(uniqueFormLocator, formName + "unique locator");
    }

}
