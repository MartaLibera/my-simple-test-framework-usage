package testFramework.baseclasses.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import testFramework.browser.BrowserFactory;
import testFramework.browser.BrowserUtils;

public class TextBox {
    private By uniqueLocator;
    private String elementName;
    private WebDriverWait waits;

    public TextBox(By locator, String name) {
        uniqueLocator = locator;
        elementName = name;
        waits = BrowserUtils.getWaits();
    }

    private WebElement getElement() {
        return BrowserFactory.createInstance().findElement(uniqueLocator);
    }

    public boolean isPresentElement(){
        try {
            waits.until(ExpectedConditions.presenceOfElementLocated(uniqueLocator));
            return true;
        }
        catch(TimeoutException e) {
            return false;
        }
    }

    public void isPresentAndEnterText(String txt) {
        isPresentElement();
        getElement().sendKeys(txt);
    }

}
