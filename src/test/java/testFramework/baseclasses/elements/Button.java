package testFramework.baseclasses.elements;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseElement;

public class Button extends BaseElement {

    public Button(By locator, String name) {
        super(locator, name);
    }

}
