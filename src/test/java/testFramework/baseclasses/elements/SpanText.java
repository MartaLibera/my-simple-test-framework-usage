package testFramework.baseclasses.elements;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseElement;

public class SpanText extends BaseElement {

    public SpanText(By locator, String name) {
        super(locator, name);
    }
}
