package testFramework.baseclasses.elements;

import org.openqa.selenium.By;
import testFramework.baseclasses.BaseElement;

public class Label extends BaseElement {

    public Label(By locator, String name) {
        super(locator, name);
    }

}
