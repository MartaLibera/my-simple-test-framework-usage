package testFramework.baseclasses;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import testFramework.browser.BrowserFactory;
import testFramework.browser.BrowserUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

public abstract class BaseTest {

    @BeforeTest
    public void setUp(){
        BrowserUtils.goToURL();
        BrowserUtils.maximizeBrowser();
    }

    public List<HashMap<String, String>> getJsonDataToMap(String filePath) throws IOException {
        String jsonContent = FileUtils.readFileToString(new File(filePath), StandardCharsets.UTF_8);

        //String to HashMap - Jackson Databind
        ObjectMapper mapper = new ObjectMapper();
        List<HashMap<String, String>> data = mapper.readValue(jsonContent, new TypeReference<List<HashMap<String, String>>>() {
        });
        return data;
    }

    @AfterTest
    public void tearDown(){
        BrowserUtils.closeBrowser();
        BrowserFactory.setDriverNull();
    }

}
