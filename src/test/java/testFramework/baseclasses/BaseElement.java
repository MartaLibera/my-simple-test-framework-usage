package testFramework.baseclasses;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;
import testFramework.browser.BrowserFactory;
import testFramework.browser.BrowserUtils;


public abstract class BaseElement {
    private By uniqueLocator;
    private String elementName;
    private WebDriverWait waits;

    static Logger log = Logger.getLogger(BaseElement.class);

    public BaseElement(By locator, String name) {
        uniqueLocator = locator;
        elementName = name;
        waits = BrowserUtils.getWaits();
    }

    private WebElement getElement() {
        return BrowserFactory.createInstance().findElement(uniqueLocator);
    }

    public void showElement() {
        log.info("element is not shown");
        WebElement element = getElement();
        ((JavascriptExecutor) BrowserFactory.createInstance()).executeScript("arguments[0].scrollIntoView(true);", element);
        log.info("element is shown");

    }

    public boolean isPresentElement(){
        log.info("Presensce of element");
        try {
            waits.until(ExpectedConditions.presenceOfElementLocated(uniqueLocator));
            return true;
        }
        catch(TimeoutException e) {
            return false;
        }
    }

    public boolean isClickableElement(){
        log.info("Trying if element is clickable");
        try {
            waits.until(ExpectedConditions.elementToBeClickable(uniqueLocator));
            return true;
        }
        catch(TimeoutException e) {
            return false;
        }
    }

    public void clickOnElement() {
        log.info("clicking on element text");
        if (!isClickableElement()) {
            log.info("element is not clickable");;
        } else {
            getElement().click();
        }
    }

    public String getElementText() {
        log.info("getting element text");
        isPresentElement();
        return getElement().getText();
    }

    public String getElementAttribute(String attrib) {
        log.info("getting elements attributes");
        isPresentElement();
        return getElement().getAttribute(attrib);
    }

}
