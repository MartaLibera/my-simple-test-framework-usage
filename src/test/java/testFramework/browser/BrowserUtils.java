package testFramework.browser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import testFramework.utilities.ConfigFileReader;

import java.time.Duration;
import java.util.ArrayList;

public class BrowserUtils {

    static ConfigFileReader configFileReader;

    public static void goToURL(){
        configFileReader = new ConfigFileReader();
        BrowserFactory.createInstance().get(configFileReader.getAppUrlValue());
    }

    public static WebDriverWait getWaits() {
        configFileReader = new ConfigFileReader();
        WebDriverWait myWait = new WebDriverWait(BrowserFactory.createInstance(),
                Duration.ofSeconds(configFileReader.getImplicitlyWaitValue()));
        return myWait;
    }

    public static void closeBrowser(){
        BrowserFactory.createInstance().quit();
    }

    public static void maximizeBrowser(){
        BrowserFactory.createInstance().manage().window().maximize();
    }


    //utilities - working with Alerts:
    public static void acceptAlert() {
        BrowserFactory.createInstance().switchTo().alert().accept();
    }

    public static String alertText() {
        return BrowserFactory.createInstance().switchTo().alert().getText();
    }

    public static void inputAlertText(String text){
        BrowserFactory.createInstance().switchTo().alert().sendKeys(text);
    }

    public void dismissAlert() {
        BrowserFactory.createInstance().switchTo().alert().dismiss();
    }

    public static boolean isAlertDisplayed() {
        try {
            BrowserFactory.createInstance().switchTo().alert().getText();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    //utilities - working with Tabs:
    public static void goToNewTab(){
        ArrayList<String> wid = new ArrayList<String>(BrowserFactory.createInstance().getWindowHandles());
        BrowserFactory.createInstance().switchTo().window(wid.get(1));
    }

    public static void closeTab(){
        BrowserFactory.createInstance().close();
    }

    public static void switchToMainTab(){
        ArrayList<String> wid = new ArrayList<String>(BrowserFactory.createInstance().getWindowHandles());
        BrowserFactory.createInstance().switchTo().window(wid.get(0));
    }

    //utilities - working with iFrames:
    public static void goToUpperFrame(WebElement upperIFrame) {
        BrowserFactory.createInstance().switchTo().frame(upperIFrame);
    }

    public static void goToLowerFrame(WebElement lowerIFrame) {
        BrowserFactory.createInstance().switchTo().frame(lowerIFrame);
    }

    public static void switchToDefaultContent() {
        BrowserFactory.createInstance().switchTo().defaultContent();
    }

}
