package testFramework.browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import testFramework.utilities.ConfigFileReader;


public class BrowserFactory {
    private static WebDriver driver;
    static ConfigFileReader configFileReader;


    public static String BrowserName() {
        configFileReader = new ConfigFileReader();
        return configFileReader.getRequestedBrowser();
    }

    public static WebDriver createInstance() {
        SupportedBrowserList browserType = SupportedBrowserList.valueOf(BrowserName().toUpperCase());
        if (driver == null) {
            switch (browserType) {
                case CHROME -> {
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                }
                case FIREFOX -> {
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                }
                default -> throw new BrowserNotSupportedException(BrowserName());
            }
        }
        return driver;
    }

    public static WebDriver setDriverNull() {
        return driver = null;
    }

}
