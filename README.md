# My Simple Test framework + usage
Implementation of simple test framework and auto tests, based on provided testcase.

## Tech description

**Testing library**: Java - TestNG, Selenium WebDriver


**Basic framework include:**
- BaseForm class;
- Base Element class and child classes for elements;
- Singleton & BrowserFactory (Factory method) to organize process of getting diver instance;
- Utility class to work wih driver;
- ConfigManager class;

**Browser**: tests works on both Chrome and Firefox browsers;



## Test cases

### 1. Alerts test

| Step | Expected result |
| ------ | ------ |
|    Navigate to main page    | Main page is open.       |
|     Click on _Alerts, Frame & Windows_   |   Alerts form has appeared on page.     |
|     Click on _Click Button_ to see alert button   |   Alert with text "You clicked a button" is open     |
|     Click _OK_ button   |   Alert has closed.    |
|     Click _On button click, confirm box will appear_ button   |   Alert with text "Do you confirm action?" is open.    |
|     Click _OK_ button   |   Alert has closed. <br><br> Text "You selected OK" has appeared on page.    |
|     Click _On button click, prompt box will appear_ button   |   Alert with text "Please enter your name" is open.    |
|     Enter randomly generated text, click _OK_ button   |   Alert has closed. <br><br> Appeared text equals to the one you've entered before.    |

### 2. Iframe test

| Step | Expected result |
| ------ | ------ |
|    Navigate to main page    |   Main page is open.     |
|    Click on _Alerts, Frame & Windows_ button <br> In a menu click _Nested Frames_ button    |     Page with Nested Frames form is open. <br><br> There are messages "Parent frame" & "Child Iframe" present on page   |
|    Select _Frames_ option in a left menu    |    Page with Frames form is open.<br><br> Message from upper frame is equal to the message from lower frame.    |

### 3. Tables test - works with two sets of data

| Step | Expected result |
| ------ | ------ |
|  Navigate to main page      |     Main page is open.    |
|    Click on _Elements_ button <br> In a menu click _Web Tables_ button    |    Page with Web Tables form is open.    |
|  Click on _Add_ button     |     _Registration Form_ has appeared on page.    |
|  Enter data for User 1 from the table (resources - json) and the click _Submit_ button      |     Registration form has closed. <br><br> Data od User 1 has appeared in a table.   
|  Click _Delete_ button in a row which contains data of User 1   |     Number of records in table has changed. <br><br>Data of User 1 has been deleted from table.     |

### 4. Handles test

| Step | Expected result |
| ------ | ------ |
|    Navigate to main page     |    Main page is open.     |
|      Click on _Alerts, Frame & Windows_ button <br> In a menu click _Browser Windows_ button  |  Page with _Browser Windows_ form is open.      |
|    Click on _New Tab_ button    |   New tab with sample page is open.     |
|   Close current tab     |   Page with _Browser Windows_ form is open.     |
|   In the menu on the left click _Elements -> Links_ button    |    Page with _Links_ form is open.    |
|    Click on _Home_ link    |   New tab with main page is open.     |
|  Resume to previous tab      |  Page with _Links_ form is open.      |



